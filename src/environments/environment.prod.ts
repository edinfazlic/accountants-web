export const environment = {
  production: true,
  api: 'https://accountants-api.herokuapp.com/'
};
